package com.vitsensors.rest

import com.vitsensors.model.sensor.entities.SensorData
import com.vitsensors.rest.dto.PostSensorDataResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface Endpoints {

    @POST("api/sensors/sensor-endpoint")
    fun postSensorData(@Body sensorData: SensorData): Call<PostSensorDataResponse>

}