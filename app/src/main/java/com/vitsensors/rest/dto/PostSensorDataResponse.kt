package com.vitsensors.rest.dto

data class PostSensorDataResponse (val int: Int)