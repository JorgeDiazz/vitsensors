package com.vitsensors.di

import com.vitsensors.MainActivity
import com.vitsensors.view.BluetoothConnectionFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(activity: MainActivity)
    fun inject(bluetoothConnectionFragment: BluetoothConnectionFragment)
}