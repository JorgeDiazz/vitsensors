package com.vitsensors.di

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.vitsensors.VITSensorsApplication
import com.vitsensors.model.bluetooth.observable.BluetoothConnectionObservable
import com.vitsensors.model.bluetooth.repository.BluetoothConnectionRepositoryImpl
import com.vitsensors.model.bluetooth.observable.IBluetoothConnectionObservable
import com.vitsensors.model.bluetooth.repository.IBluetoothConnectionRepository
import com.vitsensors.rest.Endpoints
import com.vitsensors.rest.ServiceBuilder
import com.vitsensors.viewmodel.bluetooth_connection.BluetoothConnectionViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class AppModule(private var VITSensorsApplication: VITSensorsApplication) {

    @Provides
    @Singleton
    fun provideApp(): VITSensorsApplication = VITSensorsApplication

    @Provides
    @Singleton
    fun provideContext(): Context = VITSensorsApplication.applicationContext

    @Provides
    @Named("bluetoothConnectionViewModelFactory")
    fun provideBluetoothConnectionViewModelFactory(bluetoothConnectionViewModelFactory: BluetoothConnectionViewModelFactory): ViewModelProvider.Factory =
        bluetoothConnectionViewModelFactory


    @Provides
    @Singleton
    fun provideEndpoints(): Endpoints = ServiceBuilder.buildService(Endpoints::class.java)

    @Provides
    fun provideIBluetoothConnectionRepository(endpoints: Endpoints): IBluetoothConnectionRepository =
        BluetoothConnectionRepositoryImpl(
            endpoints
        )

    @Provides
    fun provideIBluetoothConnectionObservable(iBluetoothConnectionRepository: IBluetoothConnectionRepository): IBluetoothConnectionObservable =
        BluetoothConnectionObservable(iBluetoothConnectionRepository)

}