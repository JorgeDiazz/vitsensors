package com.vitsensors.view

import android.app.Activity.RESULT_OK
import android.bluetooth.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.vitsensors.R
import com.vitsensors.VITSensorsApplication
import com.vitsensors.databinding.FragmentBluetoothConnectionBinding
import com.vitsensors.utils.ui.BluetoothUtils
import com.vitsensors.utils.ui.BluetoothUtils.Companion.ConnectionStatus
import com.vitsensors.utils.ui.BluetoothUtils.Companion.REQUEST_ENABLE_BT
import com.vitsensors.viewmodel.bluetooth_connection.BluetoothConnectionViewModel
import kotlinx.android.synthetic.main.fragment_bluetooth_connection.*
import kotlinx.android.synthetic.main.header_bluetooth_status.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named


class BluetoothConnectionFragment : Fragment(), CoroutineScope by MainScope() {

    @field:[Inject Named("bluetoothConnectionViewModelFactory")]
    lateinit var bluetoothConnectionViewModelFactory: ViewModelProvider.Factory

    private lateinit var bluetoothConnectionViewModel: BluetoothConnectionViewModel
    private lateinit var binding: FragmentBluetoothConnectionBinding

    private lateinit var bluetoothAdapter: BluetoothAdapter
    private lateinit var bluetoothGatt: BluetoothGatt
    private lateinit var connectionStatus: ConnectionStatus

    private val sensorBTDeviceName = "Espressif\u0003"
    private val writeMessageServiceUUID = "0000c303-0000-1000-8000-00805f9b34fb"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bluetooth_connection, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setUpInjection()
        setUpBindings()
        setUpView()
    }

    private fun setUpInjection() {
        (activity!!.applicationContext as VITSensorsApplication).component.inject(this)
    }

    private fun setUpBindings() {
        setUpBluetoothConnectionViewModel()
    }

    private fun setUpBluetoothConnectionViewModel() {
        bluetoothConnectionViewModel =
            ViewModelProvider(this, bluetoothConnectionViewModelFactory).get(BluetoothConnectionViewModel::class.java)
        binding.bluetoothConnectionModel = bluetoothConnectionViewModel
    }

    private fun setUpView() {
        btn_search_bluetooth_networks.setOnClickListener {
            searchBluetoothNetworks()
        }

        setUpStatusBluetoothConnectionView(false)
    }

    private fun setUpStatusBluetoothConnectionView(connected: Boolean) {
        if (connected) {
            DrawableCompat.setTint(
                ic_status_bluetooth_connection.drawable,
                ContextCompat.getColor(activity!!.applicationContext, R.color.green_connected)
            )
            tv_status_bluetooth_connection.text = getString(R.string.connected)
        } else {
            DrawableCompat.setTint(
                ic_status_bluetooth_connection.drawable,
                ContextCompat.getColor(activity!!.applicationContext, R.color.red_disconnected)
            )
            tv_status_bluetooth_connection.text = getString(R.string.disconnected)
        }
    }

    private fun searchBluetoothNetworks() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        if (!bluetoothAdapter.isEnabled) {
            BluetoothUtils.requestBluetoothEnabled(activity)
        }

        getAvailableBluetoothDevices()
    }

    private fun getAvailableBluetoothDevices() {
        bluetoothConnectionViewModel.getAvailableBluetoothDevices(activity, bluetoothAdapter, bluetoothReceiver)
    }

    private val bluetoothReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            val action = intent.action

            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                val deviceName = device?.name

                if (deviceName == sensorBTDeviceName) {
                    Timber.i("Connecting to %s...", sensorBTDeviceName)
                    bluetoothGatt = BluetoothUtils.connectToBluetoothDevice(context, device, false, gattCallback)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                getAvailableBluetoothDevices()
            } else {
                Toast.makeText(context, getString(R.string.you_must_activate_bluetooth), Toast.LENGTH_LONG).show()
            }
        }
    }

    private val gattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {

            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> {
                    connectionStatus = ConnectionStatus.CONNECTED
                    bluetoothGatt.discoverServices()

                    setUpStatusBluetoothConnectionView(true)
                    Timber.i("Bluetooth device connected!")
                }

                BluetoothProfile.STATE_DISCONNECTED -> {
                    connectionStatus = ConnectionStatus.DISCONNECTED

                    setUpStatusBluetoothConnectionView(false)
                    Timber.d("Bluetooth device disconnected!")
                }
            }
        }

        private fun setUpStatusBluetoothConnectionView(status: Boolean) {
            CoroutineScope(coroutineContext).launch {
                this@BluetoothConnectionFragment.setUpStatusBluetoothConnectionView(status)
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            super.onServicesDiscovered(gatt, status)

            for (service in bluetoothGatt.services) {
                for (characteristic in service.characteristics) {
                    if (characteristic.uuid.toString() == writeMessageServiceUUID) {
                        val originalString = "This is a message to send."
                        val byteArray: ByteArray = originalString.toByteArray()

                        characteristic.value = byteArray

                        bluetoothGatt.writeCharacteristic(characteristic)

                        Timber.i("Characteristic wrote! Message: %s", characteristic.value)
                    }
                }
            }
        }

        override fun onCharacteristicRead(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            super.onCharacteristicRead(gatt, characteristic, status)

            when (status) {
                BluetoothGatt.GATT_SUCCESS -> {
                    Timber.i("Message received: %s", characteristic!!.value)
                }
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        cancel()
    }

    companion object {
        @JvmStatic
        fun newInstance() = BluetoothConnectionFragment()
    }
}