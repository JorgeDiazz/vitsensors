package com.vitsensors.viewmodel.bluetooth_connection

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import androidx.lifecycle.ViewModel
import com.vitsensors.model.bluetooth.observable.IBluetoothConnectionObservable
import javax.inject.Inject

class BluetoothConnectionViewModel @Inject constructor(private val bluetoothConnectionObservable: IBluetoothConnectionObservable) : ViewModel() {


    fun getAvailableBluetoothDevices(activity: Activity?, bluetoothAdapter: BluetoothAdapter, bluetoothReceiver: BroadcastReceiver) {
        bluetoothConnectionObservable.getAvailableBluetoothDevices(activity, bluetoothAdapter, bluetoothReceiver)
    }


}