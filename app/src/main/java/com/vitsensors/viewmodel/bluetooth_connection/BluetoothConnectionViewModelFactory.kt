package com.vitsensors.viewmodel.bluetooth_connection

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject
import javax.inject.Provider

class BluetoothConnectionViewModelFactory @Inject constructor(private val myViewModelProvider: Provider<BluetoothConnectionViewModel>) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return myViewModelProvider.get() as T
    }
}