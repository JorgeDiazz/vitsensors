package com.vitsensors

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.vitsensors.utils.ui.FragmentUtils.Companion.replaceFragment
import com.vitsensors.view.BluetoothConnectionFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUpInjection()
        setUpView()
    }

    private fun setUpInjection() {
        (applicationContext as VITSensorsApplication).component.inject(this)
    }

    private fun setUpView() {
        setUpBluetoothConnectionFragment()
    }

    private fun setUpBluetoothConnectionFragment() {
        val bluetoothConnectionFragment = BluetoothConnectionFragment.newInstance()
        replaceFragment(bluetoothConnectionFragment, R.id.fragment_container)
    }
}