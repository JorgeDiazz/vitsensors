package com.vitsensors

import android.app.Application
import com.vitsensors.di.AppComponent
import com.vitsensors.di.AppModule
import com.vitsensors.di.DaggerAppComponent
import timber.log.Timber
import timber.log.Timber.DebugTree


class VITSensorsApplication : Application() {
    val component: AppComponent by lazy {
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        setUpTimberLogging()
    }

    private fun setUpTimberLogging() {
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
    }
}