package com.vitsensors.model.bluetooth.repository

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver

interface IBluetoothConnectionRepository {
    fun getAvailableBluetoothDevices(activity: Activity?, bluetoothAdapter: BluetoothAdapter, bluetoothReceiver: BroadcastReceiver)
}