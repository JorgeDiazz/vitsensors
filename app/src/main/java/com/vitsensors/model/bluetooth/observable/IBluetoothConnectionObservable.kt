package com.vitsensors.model.bluetooth.observable

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver

interface IBluetoothConnectionObservable {
    fun getAvailableBluetoothDevices(activity: Activity?, bluetoothAdapter: BluetoothAdapter, bluetoothReceiver: BroadcastReceiver)
}