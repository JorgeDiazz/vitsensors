package com.vitsensors.model.bluetooth.repository

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.IntentFilter
import com.vitsensors.rest.Endpoints
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BluetoothConnectionRepositoryImpl @Inject constructor(private val endpoints: Endpoints) : IBluetoothConnectionRepository {

    override fun getAvailableBluetoothDevices(activity: Activity?, bluetoothAdapter: BluetoothAdapter, bluetoothReceiver: BroadcastReceiver) {
        val filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        activity?.registerReceiver(bluetoothReceiver, filter)

        bluetoothAdapter.startDiscovery()
    }
}