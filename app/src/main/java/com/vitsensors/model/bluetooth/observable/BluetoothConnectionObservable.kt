package com.vitsensors.model.bluetooth.observable

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import androidx.databinding.BaseObservable
import com.vitsensors.model.bluetooth.repository.IBluetoothConnectionRepository
import javax.inject.Inject

class BluetoothConnectionObservable @Inject constructor(private val bluetoothConnectionRepository: IBluetoothConnectionRepository) :
    BaseObservable(), IBluetoothConnectionObservable {

    override fun getAvailableBluetoothDevices(activity: Activity?, bluetoothAdapter: BluetoothAdapter, bluetoothReceiver: BroadcastReceiver) {
        bluetoothConnectionRepository.getAvailableBluetoothDevices(activity, bluetoothAdapter, bluetoothReceiver)
    }
}