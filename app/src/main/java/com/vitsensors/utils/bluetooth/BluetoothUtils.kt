package com.vitsensors.utils.ui

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCallback
import android.content.Context
import android.content.Intent

class BluetoothUtils {

    companion object {

        const val REQUEST_ENABLE_BT = 101

        fun requestBluetoothEnabled(activity: Activity?) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            activity?.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        }

        fun connectToBluetoothDevice(context: Context?, device: BluetoothDevice, autoConnect: Boolean, gattCallback: BluetoothGattCallback): BluetoothGatt {
            return device.connectGatt(context, autoConnect, gattCallback)
        }

        enum class ConnectionStatus {
            CONNECTED, DISCONNECTED
        }

    }
}